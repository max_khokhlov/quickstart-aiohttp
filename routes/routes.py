from app.controllers import main, redis

main = main.MainController()
redis = redis.RedisController()

routes = [
    (['GET', 'POST'], '/', main.index, 'index'),
    (['GET', 'POST'], '/register', redis.register_service, 'register_service'),
]