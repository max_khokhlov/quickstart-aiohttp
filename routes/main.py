import aiohttp_cors
from aiohttp import web

from app.routes.routes import routes


def init_routes(app: web.Application) -> None:

    add_route = app.router.add_route
    add_resource = app.router.add_resource
    cors = aiohttp_cors.setup(app)

    cors_options = aiohttp_cors.ResourceOptions(
            allow_credentials=False,
            expose_headers="*",
            allow_headers=("X-Requested-With", "Content-Type"),
            max_age=3600,
    )

    for route in routes:
        resource = cors.add(add_resource(route[1]))
        for method in route[0]:
            cors.add(resource.add_route(method, route[2]), {"*": cors_options})
