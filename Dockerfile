FROM python:latest
ADD . /app
WORKDIR /app
CMD ls -la
RUN pip install uvloop
RUN pip freeze > requirements.txt
RUN pip install -r requirements.txt