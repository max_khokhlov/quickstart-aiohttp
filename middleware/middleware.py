from aiohttp.web import middleware
from aiohttp import web


@middleware
async def auth(request, handler):
    print(request.app)
    response = await handler(request)
    print('Middleware 1 finished')
    return response


@middleware
async def auth2(request, handler):
    print('Middleware 3 called')
    response = await handler(request)
    print('Middleware 3 finished')
    return response


@middleware
async def error_middleware(request, handler):
    try:
        response = await handler(request)
        if response.status != 404:
            return response
        message = "test"
    except web.HTTPException as ex:
        if ex.status != 404:
            raise
        message = ex.reason
    return web.json_response({'error': message})


middleware_app = [auth, auth2]

__all__ = [
    'middleware_app'
]
