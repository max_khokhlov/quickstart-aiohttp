from aiohttp import web


class MainController:

    def __init__(self):
        pass

    @staticmethod
    async def index(request):
        data = {'some123': 'data123'}
        return web.json_response(data)
