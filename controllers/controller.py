class BaseController:

    @staticmethod
    async def get_fields(request):
        return {key: value for key, value in request.query.items()}

    @staticmethod
    async def get_headers(request):
        return {key: value for key, value in request.headers.items()}


class Controller:

    @staticmethod
    async def get_fields(request):
        return {key: value for key, value in request.query.items()}

    @staticmethod
    async def get_headers(request):
        return {key: value for key, value in request.headers.items()}
