from aiohttp import web
from app.controllers.controller import Controller
import uuid


class RedisController(Controller):

    async def register_service(self, request) -> web.Response:

        redis_conn = request.app['redis_conn']

        token = uuid.uuid3(uuid.NAMESPACE_DNS, 'python.org1234')

        set = await redis_conn.set('service', str(token))
        value = await redis_conn.get('service', encoding='utf-8')

        fields = await self.get_fields(request)
        headers = await self.get_headers(request)

        return web.json_response({"app": request.app['config']})

