from aiohttp import web
import yaml

from app.env import DEFAULT_CONFIG_PATH


def get_config(path):
    with open(path) as f:
        config = yaml.safe_load(f)
    return config


def init_config(app: web.Application) -> None:
    app['config'] = get_config(DEFAULT_CONFIG_PATH.as_posix())
