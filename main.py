from app.app import init_app


def create_app():
    print('main')
    app = init_app()
    return app


if __name__ == "__main__":
    create_app()
