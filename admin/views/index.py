from aiohttp import web
import aiohttp_jinja2


class Index(web.View):

    @aiohttp_jinja2.template('index.html')
    async def get(self):
        questions = [{"question_text": x} for x in range(10)]
        print(questions)
        return {"questions": questions}
