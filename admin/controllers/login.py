from aiohttp import web
from app.controllers.controller import BaseController


class LoginController(BaseController):

    async def is_login(self, request):
        fields = await self.get_fields(request)
        headers = await self.get_headers(request)

        return web.json_response({"fields_is_login": fields, "remote_is_login": headers})

    async def login(self, request):
        fields = await self.get_fields(request)
        headers = await self.get_headers(request)

        return web.json_response({"fields_login": fields, "remote_login": headers})

    async def logout(self, request):
        fields = await self.get_fields(request)
        headers = await self.get_headers(request)

        return web.json_response({"fields_logout": fields, "remote_logout": headers})
