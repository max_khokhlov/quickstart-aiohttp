from aiohttp import web
import aiohttp_jinja2
import jinja2

from app.env import ADMIN_TEMPLATES

# Middleware
from app.utils import config as config_utils
from app.admin.routes.main import init_routes
from app.admin.middleware.middleware import middleware_app

# Services
from app.services.redis import initialize_redis, redis_close
from app.services.database import initialize_db, db_close


def init_admin_app() -> web.Application:

    app = web.Application(middlewares=middleware_app)

    init_routes(app)
    config_utils.init_config(app)

    # Connect templates
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(ADMIN_TEMPLATES))

    app.on_startup.extend([initialize_db, initialize_redis])
    app.on_cleanup.extend([db_close, redis_close])

    return app
