from app.admin.views import index

from aiohttp import web

# Views
views_routes = [
    web.view('/', index.Index),
]
