from app.admin.controllers import login
from aiohttp import web

# Controllers
login_controller = login.LoginController()

# Api Routes
api_routes = [
    web.get('/api/isLogin', login_controller.is_login),
    web.post('/api/isLogin', login_controller.is_login),
    web.post('/api/login', login_controller.login),
    web.post('/api/logout', login_controller.logout),
]

__all__ = [
    'api_routes'
]
