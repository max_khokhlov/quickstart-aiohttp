import aiohttp_cors
from aiohttp import web

from app.env import ADMIN_ROOT

from app.admin.routes.routes import get_routes


def setup_static_routes(app):
    app.router.add_static('/static', path=ADMIN_ROOT / 'static', name='static')


def init_routes(app: web.Application) -> None:
    routes = get_routes()
    app.add_routes(routes)
    setup_static_routes(app)


