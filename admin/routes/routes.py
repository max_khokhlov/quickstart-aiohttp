import functools
from app.admin.routes import api, view

routes = [
    api.api_routes,
    view.views_routes,
]


def get_routes():
    routes_merge = functools.reduce(lambda acc, item: acc + item, routes, [])
    print(routes_merge)
    return routes_merge


__all__ = [
    'get_routes'
]
