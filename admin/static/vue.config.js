const path = require('path')
const common_webpack = require('./webpack/common.config')

module.exports = {
  publicPath: '/admin',
  transpileDependencies: [
    'vue-echarts',
    'resize-detector'
  ],
  configureWebpack: common_webpack
}

