export default {
    en: {
      title: "Login",
      description: "Welcome back, please login account",
      labelInputUserId: "User ID",
      password: "Password",
      rememberMe: "Remember Me",
      buttons: {
        login: "Login"
      }
    },
    ru: {
      title: "Вход",
      description: "Добро пожаловать!",
      labelInputUserId: "Табельный номер",
      password: "Пароль",
      rememberMe: "Запомнить меня",
      buttons: {
        login: "Вход"
      }
    }
}
