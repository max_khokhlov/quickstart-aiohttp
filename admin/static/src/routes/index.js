import Vue from 'vue'
import Router from 'vue-router'
import auth from "@/auth/authService";

import login from './login'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return {x: 0, y: 0}
  },
  routes: [
    ...login,
    {
      path: '/error-404',
      component: () => import('../components/error/pages/404Page.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: '/error-500',
      component: () => import('../components/error/pages/500Page.vue'),
      meta: {
        rule: 'editor'
      }
    },
    // Redirect to 404 page, if no match found
    {
      path: '*',
      redirect: '/error-404'
    }
  ],
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')

  if (appLoading) {
    appLoading.style.display = "none";
  }

})

export default router
