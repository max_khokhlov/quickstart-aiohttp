export default [
  {
    path: '/login',
    component: () => import('../../components/login/pages/LoginPage.vue'),
    children: [],
    meta: {
      rule: "public"
    }
  }
]
