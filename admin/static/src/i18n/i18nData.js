import login from './../components/login/lang'

export default {
  en: {
    login: login.en,
    common: {
      error404: "404 - Page Not Found!",
      buttons: {
        backToHome: "Back to Home"
      }
    }
  },
  ru: {
    login: login.ru,
    common: {
      error404: "404 - Страница не найдена!",
      buttons: {
        backToHome: "На главную"
      }
    }
  }
}
