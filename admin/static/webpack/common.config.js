const path = require('path')

module.exports = {
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
}
