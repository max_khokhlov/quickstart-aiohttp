from aiohttp import web

#Admin App
from app.admin.app import init_admin_app

# Middleware
from app.utils import config as config_utils
from app.routes.main import init_routes
from app.middleware.middleware import middleware_app

# Services
from app.services.redis import initialize_redis, redis_close
from app.services.database import initialize_db, db_close


def init_app() -> web.Application:

    app = web.Application(middlewares=middleware_app)

    init_routes(app)
    config_utils.init_config(app)

    app.on_startup.extend([initialize_db, initialize_redis])
    app.on_cleanup.extend([db_close, redis_close])

    # Add Admin Panel
    admin_app = init_admin_app()

    app.add_subapp('/admin', admin_app)

    return app
