import aioredis
from functools import partial
from aiohttp import web


async def initialize_redis(app: web.Application) -> None:

    config = app['config']['redis']

    redis_connection = await aioredis.create_redis(
        f'redis://:{config["password"]}@{config["host"]}:{config["port"]}'
    )

    create_redis = partial(
        aioredis.create_redis,
        f'redis://:{config["password"]}@{config["host"]}:{config["port"]}'
    )

    app['redis_conn'] = redis_connection
    app['create_redis'] = create_redis


async def redis_close(app: web.Application):
    app['redis_conn'].close()
