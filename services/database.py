import sqlalchemy as sa
from aiopg.sa import create_engine
from aiohttp import web

from app.core.drivers.base_driver import BaseDriver
from app.core.drivers.oracle import OracleDriver
from app.core.drivers.postgres import PostgresDriver


def get_drivers_db():

    drivers = {
        "oracle": OracleDriver,
        "postgres": PostgresDriver
    }

    return BaseDriver(drivers)


async def initialize_db(app: web.Application):
    default_driver = app['config']['database']['default_driver']
    default_connect = app['config']['database']['default_connect']

    db_driver = get_drivers_db()
    db_default = db_driver.connect(default_driver)

    engine = db_default.connect(default_connect)

    # engine = await create_engine(**config)
    # app['db'] = app.loop.

    # create_tables
    # await migrations(app)


async def db_close(app: web.Application):
    app['db'].close()
    await app['db'].wait_closed()
