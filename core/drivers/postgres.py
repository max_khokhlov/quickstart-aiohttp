from aiopg.sa import create_engine
from app.core.drivers.base_driver import BaseDriver


class PostgresDriver(BaseDriver):

    name_driver = "oracle"
    _cursor = None

    @property
    def cursor(self):
        return self._cursor

    def connect(self, name: str):
        assert (self._is_connect(name) is False), f"Error connect oracle drivers, not connect {name}"
        config_conn = self.config[self.name_driver][name]

        self._cursor = create_engine(**config_conn)

        return self.cursor


