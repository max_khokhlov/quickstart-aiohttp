from __future__ import annotations
from app.utils.config import get_config
from app.env import DEFAULT_CONFIG_PATH


class BaseDriver:

    def __init__(self, drivers: dict):
        self.drivers = drivers
        self.name_driver = None

    def initialize(self):
        pass

    def _is_connect(self, name):
        return True if name in self.config[self.name_driver].keys() else False

    @property
    def config(self):
        assert DEFAULT_CONFIG_PATH, "Not found env = DEFAULT_CONFIG_PATH to env.py"
        config = get_config(DEFAULT_CONFIG_PATH.as_posix())
        assert (config['database']['drivers'] is None), "Add config by database and write drivers to him"
        return config['database']['drivers']

    def _is_driver(self, name):
        if name in self.drivers.keys():
            return True
        return False

    def connect(self, name: str):
        print(self._is_driver(name))
        assert (self._is_driver(name) is False), "Connection not found, check config"
        return self.drivers[name]


