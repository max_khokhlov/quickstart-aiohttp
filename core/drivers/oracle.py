from __future__ import print_function
from app.core.drivers.base_driver import BaseDriver
import cx_Oracle


class OracleDriver(BaseDriver):

    name_driver = "oracle"
    _cursor: cx_Oracle

    @property
    def cursor(self):
        return self._cursor

    def connect(self, name: str):
        assert (self._is_connect(name) is False), f"Error connect oracle drivers, not connect {name}"
        config_conn = self.config[self.name_driver][name]

        self._cursor = cx_Oracle.connect(config_conn['user'], config_conn['password'], config_conn['host'])

        return self.cursor






