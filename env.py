from pathlib import Path

APP_PATH = Path(__file__).parent
DEFAULT_CONFIG_PATH = APP_PATH / 'config' / 'config.yml'

# Admin
ADMIN_ROOT = APP_PATH / 'admin'
ADMIN_TEMPLATES = ADMIN_ROOT / 'templates'


